package com.j24.security.template.service;

import com.j24.security.template.TemplateApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.mail.MessagingException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = TemplateApplication.class)
public class MailingServiceTest {
    @Autowired
    private MailingService mailingService;

    @Test
    public void testSendingEmail() {
        Assert.assertNotNull(mailingService);

        try {
            mailingService.sendEmail("pawel.reclaw@gmail.com", "Siema javagda24!", "Testing email.");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testSendingTemplateEmail() {
        Assert.assertNotNull(mailingService);

        mailingService.sendWelcomeEmail("pawel.reclaw@gmail.com");
    }

}
